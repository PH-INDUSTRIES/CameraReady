# CameraReady

This is the repository for the correspondent paper "CameraReady".

## Contents

Three different Unity-projects are included here, dependent on which devices are used.
1. "ThreeDPoseTracker TK":Standalone project which uses the webcam directly connected to the computer.
2. "ThreeDPoseTrackerServer": Client-Server project for transmitting webcam feed between two windows pcs.
3. "TKPhone": Client-Server project for a specific phone application.

## Building

All projects can be simply built per Unity Editor and are tested with Unity version 2019.3.15f1.

The TKPhone project can be built both for android and windows. Simply change in Build Settings in Unity Editor.

## References

1. Neural Network Pose Estimation: https://github.com/digital-standard/ThreeDPoseTracker
2. Microsoft Kinect SDK: https://developer.microsoft.com/en-us/windows/kinect/
3. Humanoid Avatar: https://sketchfab.com/3d-models/rp-nathan-animated-003-walking-cb593c4676144aad83711d1253727baf
4. Webcam Feed over the Network: https://github.com/BarakChamo/uTextureSendReceive
5. Mirror Unity Networking: https://mirror-networking.com/
6. Unity Editor: https://unity3d.com/unity/whats-new/2019.3.15