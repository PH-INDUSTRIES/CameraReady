﻿/// <summary>
/// This struct defines the avatar bones with the associated Kinect-bones given in the json-files
/// </summary>
public struct BoneStruct
{

    // Bones are the avatar's joints defining a bone
    public string bone;
    public string nextBone;
    // Joints are the Kinect's joints defining a bone
    public string nextJoint;
    public string currentJoint;

    /// <summary>
    /// BoneStruct constructor
    /// </summary>
    /// <param name="bone">Beginning Joint of an avatar's bone</param>
    /// <param name="nextBone">Ending Joint of the same bone</param>
    /// <param name="nextJoint">Ending Joint of the Kinect json-file</param>
    /// <param name="currentJoint">Beginning Joint of the same Kinect bone</param>
    public BoneStruct(string bone, string nextBone, string nextJoint, string currentJoint)
    {
        this.bone = bone;
        this.nextBone = nextBone;
        this.nextJoint = nextJoint;
        this.currentJoint = currentJoint;
    }
    
    
}
