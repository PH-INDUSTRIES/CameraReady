﻿using Mirror;
using UnityEngine;

/// <summary>
/// This script's only purpose is to group all joints and bones onto one parent
/// </summary>
public class GrouperScript : NetworkBehaviour
{
    private GameObject bonesNjoints;

    // Start is called before the first frame update
    void Start()
    {
        if (!isServer)
        {
            bonesNjoints = GameObject.Find("Bones_n_Joints_Client");
            transform.SetParent(bonesNjoints.transform);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
