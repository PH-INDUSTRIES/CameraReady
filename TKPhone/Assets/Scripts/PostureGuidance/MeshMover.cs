﻿using Mirror;
using UnityEngine;

/// <summary>
/// This script manages the real-world movement and copies it to the phone
/// </summary>
public class MeshMover : NetworkBehaviour
{
    public GameObject mesh;
    public Material outlineMaterial;
    
    
    
    
    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    /// <summary>
    /// (DEPRECATED)
    /// Used to update the visibility of the stick figure on the phone
    /// </summary>
    /// <param name="status">true, if stick-figure should be visible</param>
    [ClientRpc]
    public void RpcChangeJointsStatus(bool status)
    {
        ChangeJointsStatus(status);
    }

    /// <summary>
    /// (DEPRECATED)
    /// Used to update the visibility of the mesh on the phone
    /// </summary>
    /// <param name="status">true, if stick-figure should be visible</param>
    [ClientRpc]
    public void RpcChangeMeshStatus(bool status)
    {
        ChangeMeshStatus(status);
    }
    
    /// <summary>
    /// (DEPRECATED)
    /// Used to update the visibility of the outline representation on the tablet
    /// </summary>
    /// <param name="status">true, if stick-figure should be visible</param>
    [ClientRpc]
    public void RpcChangeOutline()
    {
        ChangeOutline();
    }

    /// <summary>
    /// (DEPRECATED)
    /// Change the visibility of the stick-figures (Use only after ClientRPC call)
    /// </summary>
    /// <param name="status">true, if stick-figure should be visible</param>
    public void ChangeJointsStatus(bool status)
    {
        gameObject.SetActive(status);
    }

    /// <summary>
    /// (DEPRECATED)
    /// Change the visibility of the mesh (Use only after ClientRPC call)
    /// </summary>
    /// <param name="status">true, if mesh should be visible</param>
    public void ChangeMeshStatus(bool status)
    {
        mesh.gameObject.SetActive(status);
    }
    
    /// <summary>
    /// (DEPRECATED)
    /// Trigger outline visualization (Use only after ClientRPC call)
    /// </summary>
    public void ChangeOutline()
    {
        SkinnedMeshRenderer mr = GameObject.Find("rp_nathan_animated_003_walking_geo")
            .GetComponent<SkinnedMeshRenderer>();
        mr.material = outlineMaterial;
        mr.material.SetColor("_Color", new Color(0, 0, 0, 1));
        mr.material.SetFloat("_Outline", 0.05f);
    }
    
    
    
    
    /// <summary>
    /// Move mesh's position (Use only after ClientRPC call)
    /// </summary>
    /// <param name="meshPos">avatar's hip position</param>
    private void MoveMesh(Vector3 meshPos)
    {
        mesh.transform.position = meshPos;
    }

    /// <summary>
    /// Move stick-figure's position (Use only after ClientRPC call)
    /// </summary>
    /// <param name="meshPos">avatar's hip position</param>
    private void MoveJoints(Vector3 jointPos)
    {
        transform.position = jointPos;
    }
    
    /// <summary>
    /// Update stick figure position on phone
    /// </summary>
    /// <param name="pos">avatar's hip position</param>
    [ClientRpc]
    public void RpcSendJointPos(Vector3 pos)
    {
        MoveJoints(pos);
    }
    
    /// <summary>
    /// Update mesh position on tablet
    /// </summary>
    /// <param name="pos">avatar's hip position</param>
    [ClientRpc]
    public void RpcSendMeshPos(Vector3 pos)
    {
        MoveMesh(pos);
    }
    
    
    
}
