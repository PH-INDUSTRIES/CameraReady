﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

namespace TextureSendReceive {

    /// <summary>
    /// Manages Webcam feed over the network. 
    /// For further documentation please visit
    /// <see cref="https://github.com/BarakChamo/uTextureSendReceive"/>
    /// </summary>
    public class WebcamManager : MonoBehaviour {
        [HideInInspector]
        public int webcamIndex = 0;
        public int width = 1280;
        public int height = 960;
        public int fps = 30;

        [HideInInspector]
        public WebCamTexture texture;
        


        public RawImage SourceImage;
        Texture2D sendTexture;
        bool ready = false;
        private bool initiated = false;

        private int sendCounter = 0;

        // Use this for initialization
        void Start () {
            // List connected cameras in console

            //SetSelectedWebcam();
        }

        public Texture2D SetSelectedWebcam() {
            // Stop previous webcam connection
            if(texture != null) texture.Stop();

            // assign and start selected webcam
            WebCamDevice[] devices = WebCamTexture.devices;
            //texture = new WebCamTexture(devices[devices.Length-1].name);

			
            texture = new WebCamTexture(devices[devices.Length-1].name, width, height, fps);
            
            sendTexture = new Texture2D(width, height);
            texture.Play();
            
            
            SourceImage.texture = texture;
            sendTexture.SetPixels(texture.GetPixels());
            
            
            
            
            
            initiated = true;

            return sendTexture;
        }

		
        void Update () {


            if (initiated)
            {

                // update sendTexture with pixels from webcamTexture

                    // Loom.RunAsync(() =>
                    // {
                    //     sendTexture.SetPixels(texture.GetPixels());
                    // });

                    //StartCoroutine(copyCOR());

                    sendTexture.SetPixels(texture.GetPixels());
                    //sendTexture.SetPixels32(texture.GetPixels32());
                    //StartCoroutine(copyCOR());

            }
        }
		
        
        IEnumerator copyCOR()
        {
            yield return new WaitForEndOfFrame();
			
            sendTexture.SetPixels(texture.GetPixels());
        }
		

        private void OnApplicationQuit() {
            if (texture != null && texture.isPlaying) {
                texture.Stop();
            }
        }		
    }
}