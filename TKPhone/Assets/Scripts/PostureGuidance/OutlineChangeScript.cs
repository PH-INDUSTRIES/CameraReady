﻿using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

/// <summary>
/// This script's sole purpose is to change to the outline visualization
/// </summary>
public class OutlineChangeScript : NetworkBehaviour
{
    private bool outlinechanged = false;
    public Material outlineMaterial;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// call the ClientRPC function for changing to outline
    /// </summary>
    public void changeOutline()
    {
        RpcPaint();
    }
    
    
    /// <summary>
    /// Update the mesh to outline on the phone
    /// </summary>
    [ClientRpc]
    private void RpcPaint()
    {
        if (!isClient||outlinechanged) return;
        SkinnedMeshRenderer mr = GetComponent<SkinnedMeshRenderer>();
        mr.material = outlineMaterial;
        mr.material.SetColor("_Color", new Color(0, 0, 0, 1));
        mr.material.SetFloat("_Outline", 0.01f);
        outlinechanged = true;
    }
    
    
}