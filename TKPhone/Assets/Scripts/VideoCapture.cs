﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Mirror;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoCapture : MonoBehaviour
{
    public enum Status
    {
        Stop,
        VideoPlay,
        CamPlay,
        VideoPause,
        CamPause
    }
    private Status status = Status.Stop;

    public GameObject InputTexture;
    public RawImage VideoScreen;
    public GameObject VideoBackground;
    public float SourceCutScale;
    public float SourceCutX;
    public float SourceCutY;
    public LayerMask _layer;
    public bool UseWebCam = true;
    public int WebCamNum = 0;
    public VideoPlayer VideoPlayer;

    private WebCamTexture webCamTexture;
    private RenderTexture videoTexture;
    private GameObject MainTextureCamera;

    private int videoScreenWidth = 2560 * 2;
    private int bgWidth, bgHeight;
    public float SourceFps = 30f;

    private Vector3 localScale;
    public RenderTexture MainTexture { get; private set; }

    public Texture testTexture;
    private Texture2D _texture2D;
    private List<byte> tex_part;

    private bool initiated = false;


    private void Start()
    {
        tex_part = new List<byte>();
    }
    
    private void Update()
    {
        if (initiated)
        {
            VideoBackground.GetComponent<Renderer>().material.mainTexture = VideoScreen.mainTexture;
        }

    }

    public void RotateScreen()
    {
        VideoScreen.transform.localScale = new Vector3(-localScale.x, localScale.y, localScale.z);
    }
    
    
    public void Init(int bgWidth, int bgHeight)
    {
        this.bgWidth = bgWidth;
        this.bgHeight = bgHeight;

        localScale = VideoScreen.transform.localScale;

        //if (UseWebCam) CameraPlayStart();
        //else VideoPlayStart();
    }
    

    public void SetMainTextureDimensions()
    {
        //var sd = VideoScreen.GetComponent<RectTransform>();

        VideoScreen.transform.localScale = new Vector3(-localScale.x, localScale.y, localScale.z);
        VideoScreen.transform.rotation = Quaternion.Euler(new Vector3(0, 0, -90));


        SourceFps = 30f;


        
        //sd.sizeDelta = new Vector2(videoScreenWidth, videoScreenWidth * VideoScreen.mainTexture.height / VideoScreen.mainTexture.width);
        // set video screen for displaying webcam
        var aspect = (float) VideoScreen.mainTexture.width / VideoScreen.mainTexture.height;
        VideoBackground.transform.localScale = new Vector3(-aspect * SourceCutScale, 1 * SourceCutScale, 1);
        VideoBackground.transform.localPosition = new Vector3(SourceCutX, SourceCutY, -2f);
        VideoBackground.GetComponent<Renderer>().material.mainTexture = VideoScreen.mainTexture;

        // MainTexture is used for the neural network estimation
        InitMainTexture();

        MainTextureCamera.transform.localPosition = new Vector3(SourceCutX, SourceCutY, -2f);
        status = Status.CamPlay;

        initiated = true;
    }

    /// <summary>
    /// Save the camera feed as jpg
    /// </summary>
    /// <param name="dir">file path</param>
    public void SaveTextureAsJPG(DirectoryInfo dir)
    {
        Texture2D tex = new Texture2D(1,1);
        tex = (Texture2D) VideoScreen.mainTexture;
        byte[] bytes = tex.EncodeToJPG();
        File.WriteAllBytes(dir.FullName+"/"+Variables.Posture.ToString()+".jpg",bytes);
    }
    
    

    public delegate void VideoReadyDelegate();
    public VideoReadyDelegate VideoReady;
    
    
    public void ResetScale(float scale, float x, float y, bool isMirror)
    {
        SourceCutScale = scale;
        SourceCutX = x;
        SourceCutY = y;
        if (VideoScreen.texture != null)
        {
            var aspect = (float)VideoScreen.texture.width / VideoScreen.texture.height;
            if(status == Status.Stop || status == Status.VideoPlay || status == Status.VideoPause)
            {
                // ビデオの場合、反転しない
            }
            else
            {
                // カメラは設定による
                if(isMirror)
                {
                    aspect =  -aspect;
                }

            }
            VideoBackground.transform.localScale = new Vector3(aspect * SourceCutScale, 1 * SourceCutScale, 1) ;

        }

        if(MainTextureCamera != null)
        {
            MainTextureCamera.transform.localPosition = new Vector3(SourceCutX, SourceCutY, -2f);
        }
    }
    
    
    public bool IsPlay()
    {
        if (status == Status.VideoPlay || status == Status.CamPlay)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool IsPause()
    {
        if (status == Status.VideoPause || status == Status.CamPause)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void PlayStop()
    {
        if (status == Status.VideoPlay || status == Status.VideoPause)
        {
            VideoPlayer.Stop();
        }
        else if (status == Status.CamPlay || status == Status.CamPause)
        {
            webCamTexture.Stop();
        }

        status = Status.Stop;
    }

    public void Pause()
    {
        if (status == Status.VideoPlay)
        {
            VideoPlayer.Pause();
            status = Status.VideoPause;
        }
        else if (status == Status.CamPlay)
        {
            webCamTexture.Pause();
            status = Status.CamPause;
        }
    }

    public void Resume()
    {
        if (status == Status.VideoPause)
        {
            VideoPlayer.Play();
            status = Status.VideoPlay;
        }
        else if (status == Status.CamPause)
        {
            webCamTexture.Play();
            status = Status.CamPlay;
        }
    }

    private void InitMainTexture()
    {
        MainTextureCamera = new GameObject("MainTextureCamera", typeof(Camera));

        MainTextureCamera.transform.parent = VideoBackground.transform;
        MainTextureCamera.transform.localScale = new Vector3(1.0f, -1.0f, 1.0f);
        MainTextureCamera.transform.localPosition = new Vector3(0.0f, 0.0f, -2.0f );
        MainTextureCamera.transform.localEulerAngles = Vector3.zero;
        MainTextureCamera.transform.rotation = Quaternion.Euler(new Vector3(0,0,90));
        MainTextureCamera.layer = _layer;

        var camera = MainTextureCamera.GetComponent<Camera>();
        camera.orthographic = true;
        camera.orthographicSize = 0.5f ;
        camera.depth = -5;
        camera.depthTextureMode = 0;
        camera.clearFlags = CameraClearFlags.Color;
        camera.backgroundColor = Color.black;
        camera.cullingMask = _layer;
        camera.useOcclusionCulling = false;
        camera.nearClipPlane = 1.0f;
        camera.farClipPlane = 5.0f;
        camera.allowMSAA = false;
        camera.allowHDR = false;

        MainTexture = new RenderTexture(bgWidth, bgHeight, 0, RenderTextureFormat.RGB565, RenderTextureReadWrite.sRGB)
        {
            useMipMap = false,
            autoGenerateMips = false,
            wrapMode = TextureWrapMode.Clamp,
            filterMode = FilterMode.Point,
        };

        camera.targetTexture = MainTexture;
        if(InputTexture.activeSelf) InputTexture.GetComponent<Renderer>().material.mainTexture = MainTexture;
    }
}
