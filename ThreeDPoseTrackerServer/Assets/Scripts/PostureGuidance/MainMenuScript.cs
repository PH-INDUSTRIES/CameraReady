﻿using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

/// <summary>
/// This script handles initial user interaction and establishes a connection between the two devices
/// </summary>
public class MainMenuScript : NetworkBehaviour
{
    public Dropdown dropdown;
    public InputField input;
    public Text posturesText;
    public Dropdown cameras;
    public Button generate;
    public Button start;
    public Button host;
    public Button connect;

    private NetworkManager nm;
    
    
    // Start is called before the first frame update
    void Start()
    {
        nm = FindObjectOfType<NetworkManager>();
        
        WebCamDevice[] devices = WebCamTexture.devices;
        foreach (var d in devices)
        {
            cameras.options.Add(new Dropdown.OptionData(d.name));
        }
        cameras.value = 0;
        cameras.gameObject.SetActive(false);
    }

    /// <summary>
    /// Function is called if the client connects to a server
    /// This is only used to hide certain UI-elements
    /// </summary>
    public void onConnect()
    {
        cameras.gameObject.SetActive(true);
        host.gameObject.SetActive(false);
        connect.gameObject.SetActive(false);
    }


    /// <summary>
    /// Function is called if a client connects to the server
    /// This is only used to hide certain UI-elements
    /// </summary>
    public void OnHost()
    {
        dropdown.gameObject.SetActive(true);
        start.gameObject.SetActive(true);
        host.gameObject.SetActive(false);
        connect.gameObject.SetActive(false);
    }



    // Update is called once per frame
    void Update()
    {
        
    }
    /// <summary>
    /// Saves the visualisations choice in the Variables script
    /// </summary>
    public void changeVis()
    {
        Variables.Version = dropdown.value;
    }

    /// <summary>
    /// This is a legacy function used for generating a random amount of postures dependent on a user given amount
    /// (This function is not used due to having a defined set of postures)
    /// </summary>
    public void onGeneratePostures()
    {
        List<int> postures = new List<int>();
        int amount = int.Parse(input.text);
        if (amount > 0 && amount < 40)
        {
            while (postures.Count<amount)
            {
                int rand = Random.Range(0, 39);
                if (postures.Count == 0 || !postures.Contains(rand))
                {
                    postures.Add(rand);
                }
            }

            string posturesString = "";
            foreach (var posture in postures)
            {
                posturesString += (posture+1).ToString() + " ,";
            }

            posturesText.text = posturesString;
            Variables.PostureArray = postures;
        }
        
    }

    /// <summary>
    /// Initializes the main program and changes to the main scene
    /// </summary>
    public void onNewStart()
    {
        List<int> postures = new List<int>();
        int amount = 20;
        for (int i = 0; i < amount; i++)
        {
            postures.Add(i);
        }

        int posTemp = 0;
        
        for (int i = 0; i < postures.Count; i++) {
            int rnd = Random.Range(0, postures.Count);
            posTemp = postures[rnd];
            postures[rnd] = postures[i];
            postures[i] = posTemp;
        }
        
        // Postures are copied to the Variables array
        Variables.PostureArray = postures;

        foreach (var VARIABLE in Variables.PostureArray)
        {
            Debug.Log(VARIABLE);
        }    

        // Load the Main Scene per Network Manager
        nm.ServerChangeScene("MainScene");
    }
    
    
    public void onStart()
    {
        nm.ServerChangeScene("MainScene");
        //SceneManager.LoadScene("MainScene");
    }
    
    /// <summary>
    /// Saves the camera choice in the Variables script
    /// (Cameras are set in the Start function)
    /// </summary>
    public void onChangeCamera()
    {
        CmdOnCameraChange();
        RpcSendCamera(cameras.value);
    }

    /// <summary>
    /// Sets the camera on the Server, called from the Tablet
    /// </summary>
    [Command]
    private void CmdOnCameraChange()
    {
        Variables.Camera = cameras.value;
    }

    /// <summary>
    /// This function is used to tell the tablet which camera to use
    /// </summary>
    /// <param name="camera">Index of the camera used</param>
    [ClientRpc]
    private void RpcSendCamera(int camera)
    {
        Variables.Camera = camera;
    }
    
}
