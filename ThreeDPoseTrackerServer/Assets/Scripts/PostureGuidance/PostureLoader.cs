﻿using System;
using System.Collections.Generic;
using System.IO;
using Mirror;
using Unity.Mathematics;
using UnityEngine;

/// <summary>
/// Main Script for managing the loading of postures
/// </summary>
public class PostureLoader : NetworkBehaviour
{
    // avatars
    private GameObject avatar;
    public GameObject yukihiko;
    // empty game-objects as parents for the stick-figure game-objects
    public GameObject stickFigureJoints;
    public GameObject stickFigureBones;

    // Joint and Bones Prefabs needed for network spawning
    public GameObject JointPrefab;
    public GameObject BonePrefab;
    
    // position of the perceived real-world position
    private Vector3 spineCorrection;
    private Vector3 hipPosition;

    // network mesh mover
    public MeshMover MeshMover;

    // mesh materials
    public Material outlinematerial;
    public Material meshMaterial;

    // elongation of the legs
    private Dictionary<string, Vector3> oldLegJoints = new Dictionary<string, Vector3>();
    private float legCorrection = 1.25f;
    
    // network outline changer
    public OutlineChangeScript ocs;
    
    // struct for managing avatar and stick-figure bones
    private List<BoneStruct> BoneStructs = new List<BoneStruct>();

    // json-string of all postures in the folder
    private string[] posturesString;
    
    // dictionary for stick-figure joints and bones
    private Dictionary<string, GameObject> spheres = new Dictionary<string, GameObject>();
    private Dictionary<string, GameObject> capsules = new Dictionary<string, GameObject>();

    // stick figure materials
    public Material joint_material;
    public Material bone_material;
    
    // hard-coded scales for stick.figure joints (to look more realistic)
    private Dictionary<string, float> sphereScales = new Dictionary<string, float>()
    {
        { "SPINEBASE", 0.15f },
        { "HIPLEFT", 0.1f },
        { "KNEELEFT", 0.1f },
        { "ANKLELEFT", 0.125f },
        { "FOOTELFT", 0.09f },
        
        { "SPINEMID", 0.13f },
        { "SPINESHOULDER", 0.1f },
        { "SHOULDERLEFT", 0.1f },
        { "ELBOWLEFT", 0.1f },
        { "WRISTLEFT", 0.07f },
        { "THUMBLEFT", 0.05f },
        { "HANDLEFT", 0.05f },
        { "HANDTIPLEFT", 0.06f },
        
        { "NECK", 0.125f },
        { "HEAD", 0.25f },
        
        { "SHOULDERRIGHT", 0.1f },
        { "ELBOWRIGHT", 0.1f },
        { "WRISTRIGHT", 0.07f },
        { "THUMBRIGHT", 0.05f },
        { "HANDRIGHT", 0.05f },
        { "HANDTIPRIGHT", 0.06f },
        
        { "HIPRIGHT", 0.1f },
        { "KNEERIGHT", 0.1f },
        { "ANKLERIGHT", 0.125f },
        { "FOOTRIGHT", 0.09f },
    };
    

    /// <summary>
    /// Used for loading json
    /// </summary>
    [Serializable]
    public class Posture
    {
        public string name;
        public float X;
        public float Y;
        public float Z;
        public Posture[] child_joints;

        /// <summary>
        /// Used for loading json
        /// </summary>
        /// <param name="name">posture name form json</param>
        /// <param name="pos">3D position</param>
        /// <param name="child_joints">Array of child joints</param>
        public Posture(string name, Vector3 pos, Posture[] child_joints)
        {
            this.name = name;
            X = pos.x;
            Y = pos.y;
            Z = pos.z;
            this.child_joints = child_joints;
        }
    }

    // Start is called before the first frame update
    public override void OnStartServer()
    {
        // only initialize if it is the server
        if (isServer)
        {
            initBoneLibrary();

            avatar = GameObject.Find("rp_nathan_animated_003_walking");

            hipPosition = GameObject.Find("rp_nathan_animated_003_walking_hip").transform.position;

            //Instantiate GameObject Groups
            InitializeArrays();

            //Load JSON files
            DirectoryInfo dir = new DirectoryInfo(Application.streamingAssetsPath + "/");
            FileInfo[] info = dir.GetFiles("*.json");
            posturesString = new string[info.Length];

            //save JSON files in string array
            for (int i = 0; i < info.Length; i++)
            {
                posturesString[i] = info[i].ToString();
            }



            changePosture(true); 
        }

    }

    /// <summary>
    /// Clear and re-initialize stickFigureJoints and stickFigureBones and subsequent dictionaries
    /// </summary>
    void InitializeArrays()
    {
        // Instant reloading requires DestroyImmediate and not Destroy
        DestroyImmediate(stickFigureJoints);
        DestroyImmediate(stickFigureBones);
        
        // Re-Initialize
        stickFigureJoints = new GameObject("Stick_Figure_Joints");
        stickFigureBones = new GameObject("Stick_Figure_Bones");
        
        // Clear the dictionaries as well
        spheres = new Dictionary<string, GameObject>();
        capsules = new Dictionary<string, GameObject>();
    }

    /// <summary>
    /// Associate the avatar's bones to the Kinect's bones from the json-file
    /// </summary>
    private void initBoneLibrary()
    {
                //Dictionary for Real-Life-Mesh representation -------------- NATHAN
        BoneStructs.Add(new BoneStruct("rp_nathan_animated_003_walking_hip","rp_nathan_animated_003_walking_spine_01","SPINEMID","SPINEBASE"));
        BoneStructs.Add(new BoneStruct("rp_nathan_animated_003_walking_spine_01","rp_nathan_animated_003_walking_neck","SPINESHOULDER","SPINEMID"));
        BoneStructs.Add(new BoneStruct("rp_nathan_animated_003_walking_neck","rp_nathan_animated_003_walking_head","HEAD","NECK"));
        
        // Joints are swapped for no reason
        BoneStructs.Add(new BoneStruct("rp_nathan_animated_003_walking_shoulder_r","rp_nathan_animated_003_walking_upperarm_r","SPINESHOULDER","SHOULDERLEFT"));
        BoneStructs.Add(new BoneStruct("rp_nathan_animated_003_walking_upperarm_r","rp_nathan_animated_003_walking_lowerarm_r","SHOULDERLEFT","ELBOWLEFT"));
        BoneStructs.Add(new BoneStruct("rp_nathan_animated_003_walking_lowerarm_r","rp_nathan_animated_003_walking_hand_r","ELBOWLEFT","WRISTLEFT"));
        BoneStructs.Add(new BoneStruct("rp_nathan_animated_003_walking_hand_r","rp_nathan_animated_003_walking_middle_01_r","WRISTLEFT","HANDLEFT"));
        BoneStructs.Add(new BoneStruct("rp_nathan_animated_003_walking_middle_01_r","rp_nathan_animated_003_walking_middle_01_r","HANDLEFT","HANDTIPLEFT")); 
        
        BoneStructs.Add(new BoneStruct("rp_nathan_animated_003_walking_shoulder_l","rp_nathan_animated_003_walking_upperarm_l","SHOULDERRIGHT","SPINESHOULDER"));
        BoneStructs.Add(new BoneStruct("rp_nathan_animated_003_walking_upperarm_l","rp_nathan_animated_003_walking_lowerarm_l","ELBOWRIGHT","SHOULDERRIGHT"));
        BoneStructs.Add(new BoneStruct("rp_nathan_animated_003_walking_lowerarm_l","rp_nathan_animated_003_walking_hand_l","WRISTRIGHT","ELBOWRIGHT"));
        BoneStructs.Add(new BoneStruct("rp_nathan_animated_003_walking_hand_l","rp_nathan_animated_003_walking_middle_01_l","HANDRIGHT","WRISTRIGHT"));
        BoneStructs.Add(new BoneStruct("rp_nathan_animated_003_walking_middle_01_l","rp_nathan_animated_003_walking_middle_01_l","HANDTIPRIGHT","HANDRIGHT")); 

        
        BoneStructs.Add(new BoneStruct("rp_nathan_animated_003_walking_upperleg_r","rp_nathan_animated_003_walking_lowerleg_r","KNEELEFT","HIPLEFT"));
        BoneStructs.Add(new BoneStruct("rp_nathan_animated_003_walking_lowerleg_r","rp_nathan_animated_003_walking_foot_r","ANKLELEFT","KNEELEFT"));
        
        // Joints are swapped for no reason
        BoneStructs.Add(new BoneStruct("rp_nathan_animated_003_walking_upperleg_l","rp_nathan_animated_003_walking_lowerleg_l","HIPRIGHT","KNEERIGHT"));
        BoneStructs.Add(new BoneStruct("rp_nathan_animated_003_walking_lowerleg_l","rp_nathan_animated_003_walking_foot_l","KNEERIGHT","ANKLERIGHT"));
    }

    /// <summary>
    /// Handle all posture loading
    /// </summary>
    /// <param name="neutral">true, if neutral pose should be shown</param>
    public void changePosture(bool neutral)
    {
        // only change posture if script is handled by server
        if (isServer)
        {

            InitializeArrays();

            //Process JSONs and instantiate stick.figure representation
            if (neutral)
            {
                LoadPosture(posturesString[20]);
            }
            else
            {
                LoadPosture(posturesString[Variables.PostureArray[Variables.Posture]]);
                //LoadPosture(posturesString[0]);
            }





            //Stick-Figure Representation
            if (Variables.Version == 0)
            {
                stickFigureBones.SetActive(true);
                stickFigureJoints.SetActive(true);


                avatar.SetActive(false);
                //MeshMover.RpcChangeMeshStatus(false);
            }
            else
                //Real-Life-Model Representation
            {
                avatar.SetActive(true);
                //MeshMover.RpcChangeMeshStatus(true);


                //Align Real-Life-Mesh with Points loaded form JSON
                foreach (var bone in BoneStructs)
                {
                    AlignBody(bone);
                }


                SkinnedMeshRenderer mr = GameObject.Find("rp_nathan_animated_003_walking_geo")
                    .GetComponent<SkinnedMeshRenderer>();
                mr.material = meshMaterial;
                stickFigureBones.SetActive(false);
                stickFigureJoints.SetActive(false);
                //MeshMover.RpcChangeJointsStatus(false);

                //Real-Life-Model Silhouette Representation
                if (Variables.Version == 2)
                {
                    mr.material = outlinematerial;
                    mr.material.SetColor("_Color", new Color(0, 0, 0, 1));
                    mr.material.SetFloat("_Outline", 0.05f);
                }
            }
        }
    }
    
    /// <summary>
    /// Reads the json-file and initializes recursive joint loading
    /// </summary>
    /// <param name="file">full file path as string</param>
    void LoadPosture(string file)
    {
            //Read File and convert it using Json
            StreamReader sr = new StreamReader(file);
            string jsonString = sr.ReadToEnd();
            Posture postureData = JsonUtility.FromJson<Posture>(jsonString);
            
            oldLegJoints.Clear();
            // start recursive loading of all joints
            setCoordinates(postureData);

            // better leg representation due to the Kinect bones not being long enough
            setCapsuleTransform(GameObject.Find("HIPLEFT").transform.position, GameObject.Find("KNEELEFT").transform.position, GameObject.Find("BONE_HIPLEFT_KNEELEFT"));
            setCapsuleTransform(GameObject.Find("KNEELEFT").transform.position, GameObject.Find("ANKLELEFT").transform.position, GameObject.Find("BONE_KNEELEFT_ANKLELEFT"));
            setCapsuleTransform(GameObject.Find("ANKLELEFT").transform.position, GameObject.Find("FOOTELFT").transform.position, GameObject.Find("BONE_ANKLELEFT_FOOTELFT"));
            setCapsuleTransform(GameObject.Find("HIPRIGHT").transform.position, GameObject.Find("KNEERIGHT").transform.position, GameObject.Find("BONE_HIPRIGHT_KNEERIGHT"));
            setCapsuleTransform(GameObject.Find("KNEERIGHT").transform.position, GameObject.Find("ANKLERIGHT").transform.position, GameObject.Find("BONE_KNEERIGHT_ANKLERIGHT"));
            setCapsuleTransform(GameObject.Find("ANKLERIGHT").transform.position, GameObject.Find("FOOTRIGHT").transform.position, GameObject.Find("BONE_ANKLERIGHT_FOOTRIGHT"));

    }

    /// <summary>
    /// Recursive loading of all Kinect-joints and setting the 3D positions of all joints and bones
    /// </summary>
    /// <param name="posture">current joint</param>
    void setCoordinates(Posture posture)
    {
        // Let the stick-figure follow the real-world position
        if (posture.name=="SPINEBASE")
        {
            spineCorrection = new Vector3(posture.X-hipPosition.x,posture.Y-hipPosition.y,posture.Z-hipPosition.z);
            //spineCorrection = new Vector3(posture.X,posture.Y+0.2f,posture.Z-1.9f);
        }
        
        //Create Spheres, set Transform and Name and save in Dictionary
        GameObject sphere = Instantiate(JointPrefab);
        float scale = sphereScales[posture.name];
        sphere.transform.localScale = new Vector3(scale,scale,scale);
        // Elongate the legs
        if(posture.name== "KNEERIGHT"|| posture.name == "ANKLERIGHT" || posture.name == "KNEELEFT" || posture.name == "ANKLELEFT")
        {
            setLegScale(posture,sphere);
        }
        else
        {
            sphere.transform.position = (new Vector3(posture.X, posture.Y, posture.Z)) - spineCorrection;
        }
        if(posture.name == "FOOTELFT")
        {
            Vector3 offset = GameObject.Find("ANKLELEFT").transform.position - oldLegJoints["ANKLELEFT"];
            sphere.transform.position += offset;
        }
        if (posture.name == "FOOTRIGHT")
        {
            Vector3 offset = GameObject.Find("ANKLERIGHT").transform.position - oldLegJoints["ANKLERIGHT"];
            sphere.transform.position += offset;
        }
        sphere.name = posture.name;
        sphere.GetComponent<MeshRenderer>().material = new Material(joint_material);

        // let the server spawn the sphere
        sphere.transform.SetParent(stickFigureJoints.transform);
        if (Variables.Version==0)
        {
            NetworkServer.Spawn(sphere);
        }
        spheres.Add(posture.name,sphere);

        // Create all further child joints
        for (int i = 0; i < posture.child_joints.Length; i++)
        {
            //Create Capsules
            //GameObject capsule = GameObject.CreatePrimitive(PrimitiveType.Capsule);
            GameObject capsule = Instantiate(BonePrefab);
            capsule.transform.localScale = new Vector3(0.03f,1f,0.03f);
            capsule.name = "BONE_" + posture.name + "_" + posture.child_joints[i].name;
            //Calculate Capsule Positions and Rotations
            Vector3 pos1 = new Vector3(posture.X,posture.Y,posture.Z);
            Vector3 pos2 = new Vector3(posture.child_joints[i].X,posture.child_joints[i].Y,posture.child_joints[i].Z);
            setCapsuleTransform(pos1,pos2,capsule);
            capsule.transform.position -= spineCorrection;
            capsule.GetComponent<MeshRenderer>().material = new Material(bone_material);

            capsule.transform.SetParent(stickFigureBones.transform);
            if (Variables.Version==0)
            {
                NetworkServer.Spawn(capsule);
            }


            //Add Capsule to Dictionary
            capsules.Add(capsule.name,capsule);
            
            //Recursively work through all child Joints of this Joint
            setCoordinates(posture.child_joints[i]);
        }
    }
    
    /// <summary>
    /// Sets the 3D position of one bone
    /// </summary>
    /// <param name="pos1">current joint</param>
    /// <param name="pos2">next joint</param>
    /// <param name="capsule">subsequent capsule</param>
    public void setCapsuleTransform(Vector3 pos1, Vector3 pos2, GameObject capsule)
    {
        capsule.transform.position = pos1 + (0.5f*(pos2 - pos1));
        capsule.transform.rotation = Quaternion.LookRotation(pos2-pos1);
        Vector3 eulers = capsule.transform.rotation.eulerAngles;
        capsule.transform.rotation = Quaternion.Euler(new Vector3(eulers.x+90,eulers.y,eulers.z));
        Vector3 scale = capsule.transform.localScale;
        capsule.transform.localScale = new Vector3(scale.x,0.5f*Vector3.Distance(pos1, pos2),scale.z);
    }
    
    /// <summary>
    /// Elongate the Legs
    /// </summary>
    /// <param name="posture">joint</param>
    /// <param name="sphere">subsequent sphere</param>
    void setLegScale(Posture posture,GameObject sphere)
    {
        Vector3 previousJoint = Vector3.zero;
        sphere.transform.position = (new Vector3(posture.X, posture.Y, posture.Z)) - spineCorrection;

        if (posture.name == "KNEELEFT")
        {
            previousJoint = GameObject.Find("HIPLEFT").transform.position;
            oldLegJoints.Add(posture.name, sphere.transform.position);
            Vector3 dir = sphere.transform.position - previousJoint;
            sphere.transform.position = previousJoint + (dir * legCorrection);
        }
        if (posture.name == "ANKLELEFT")
        {
            previousJoint = GameObject.Find("KNEELEFT").transform.position;
            oldLegJoints.Add(posture.name, sphere.transform.position);
            Vector3 dir = sphere.transform.position - oldLegJoints["KNEELEFT"];
            Vector3 offset = previousJoint - oldLegJoints["KNEELEFT"];
            sphere.transform.position = previousJoint + (dir * legCorrection) +offset;
        }
        if (posture.name == "KNEERIGHT")
        {
            oldLegJoints.Add(posture.name, sphere.transform.position);
            previousJoint = GameObject.Find("HIPRIGHT").transform.position;
            Vector3 dir = sphere.transform.position - previousJoint;
            sphere.transform.position = previousJoint + (dir * legCorrection);
        }
        if (posture.name == "ANKLERIGHT")
        {
            previousJoint = GameObject.Find("KNEERIGHT").transform.position;
            oldLegJoints.Add(posture.name, sphere.transform.position);
            Vector3 dir = sphere.transform.position - oldLegJoints["KNEERIGHT"];
            Vector3 offset = previousJoint - oldLegJoints["KNEERIGHT"];
            sphere.transform.position = previousJoint + (dir * legCorrection) + offset;
        }
    }
    
    
    // Update is called once per frame
    void Update()
    {
        if (isServer)
        {
            // get real-world position which is located in the avatar's hip
            Vector3 yukipos = yukihiko.transform.position;

            // relocate avatar position per MeshMover
            avatar.transform.position = yukipos;
            MeshMover.RpcSendMeshPos(yukipos);

            // relocate stick-figure location
            stickFigureBones.transform.position = yukipos;
            stickFigureJoints.transform.position = yukipos;
            MeshMover.RpcSendJointPos(yukipos);

            // always check for outline and change respectively
            if (Variables.Version == 2)
            {
                ocs.changeOutline();
            }
        }

    }

    /// <summary>
    /// Align the avatar's mesh to the stick-figure
    /// </summary>
    /// <param name="bs">one bone from the bone struct</param>
    public void AlignBody(BoneStruct bs)
    {
        GameObject bone = GameObject.Find(bs.bone);
        //GameObject nextBone = GameObject.Find(bs.nextBone);
        GameObject nextJoint = GameObject.Find(bs.nextJoint);
        GameObject currentJoint = GameObject.Find(bs.currentJoint);
        
        // final bone rotation
        Quaternion rottt = new Quaternion();

        // rotation of one bone
        Vector3 aimDir = currentJoint.transform.position - nextJoint.transform.position;
        
        // rotation in respect to the horizontal plane
        Vector3 horizontalAngle = new Vector3(aimDir.x,0,aimDir.z);
        float angle = Vector3.Angle(horizontalAngle, Vector3.forward);
        if (aimDir.x < 0)
        {
            angle = -angle;
        }

        // multiple bones did always have incorrect rotations, so an additional rotation was necessary
        Vector3 correctionVector = new Vector3(angle,-90,0);

        // Some bones were always switched around
        if (bs.bone == "rp_nathan_animated_003_walking_shoulder_r"||bs.bone == "rp_nathan_animated_003_walking_upperarm_r"||bs.bone == "rp_nathan_animated_003_walking_lowerarm_r")
        {
            correctionVector += new Vector3(180,0,0);
        }

        // set the left middle fingers' rotations
        if (bs.bone == "rp_nathan_animated_003_walking_hand_r")
        {
            Vector3 a = GameObject.Find("WRISTLEFT").transform.position;
            Vector3 b = GameObject.Find("HANDLEFT").transform.position;
            Vector3 c = GameObject.Find("THUMBLEFT").transform.position;
            Vector3 hand_r_normal = Vector3.Cross(b - a, c - a);
            
            Vector3 e = GameObject.Find("rp_nathan_animated_003_walking_thumb_01_r").transform.position;
            Vector3 f = GameObject.Find("rp_nathan_animated_003_walking_middle_01_r").transform.position;
            Vector3 mesh_normal = Vector3.Cross(e - bone.transform.position, f - bone.transform.position);

            float normal_angle =
                math.acos(Vector3.Dot(hand_r_normal,mesh_normal) / (Vector3.Magnitude(hand_r_normal) * Vector3.Magnitude(mesh_normal)));
            normal_angle *= Mathf.Rad2Deg;
        
            correctionVector += new Vector3(normal_angle,0,0);
        }
        
        // set the right middle fingers' rotations
        if (bs.bone == "rp_nathan_animated_003_walking_hand_r")
        {
            Vector3 a = GameObject.Find("WRISTRIGHT").transform.position;
            Vector3 b = GameObject.Find("HANDRIGHT").transform.position;
            Vector3 c = GameObject.Find("THUMBRIGHT").transform.position;
            Vector3 hand_l_normal = Vector3.Cross(b - a, c - a);
            
            Vector3 e = GameObject.Find("rp_nathan_animated_003_walking_thumb_01_l").transform.position;
            Vector3 f = GameObject.Find("rp_nathan_animated_003_walking_middle_01_l").transform.position;
            Vector3 mesh_normal = Vector3.Cross(e - bone.transform.position, f - bone.transform.position);

            float normal_angle =
                math.acos(Vector3.Dot(hand_l_normal,mesh_normal) / (Vector3.Magnitude(hand_l_normal) * Vector3.Magnitude(mesh_normal)));
            normal_angle *= Mathf.Rad2Deg;
        
            correctionVector += new Vector3(normal_angle,0,0);
        }
        if (bs.bone == "rp_nathan_animated_003_walking_middle_01_r")
        {
            correctionVector += new Vector3(180,0,0);
        }



        // convert to quaternion
        rottt = Quaternion.LookRotation(aimDir)*Quaternion.Euler(correctionVector);
        // set rotation
        bone.transform.rotation = rottt;
        
        // set other fingers' rotation
        if (bs.bone == "rp_nathan_animated_003_walking_middle_01_l")
        {
            GameObject.Find("rp_nathan_animated_003_walking_index_01_l").transform.rotation = rottt;
            GameObject.Find("rp_nathan_animated_003_walking_pinky_01_l").transform.rotation = rottt;
            GameObject.Find("rp_nathan_animated_003_walking_ring_01_l").transform.rotation = rottt;
        }
        if (bs.bone == "rp_nathan_animated_003_walking_middle_01_r")
        {
            GameObject.Find("rp_nathan_animated_003_walking_index_01_r").transform.rotation = rottt;
            GameObject.Find("rp_nathan_animated_003_walking_pinky_01_r").transform.rotation = rottt;
            GameObject.Find("rp_nathan_animated_003_walking_ring_01_r").transform.rotation = rottt;
        }

        
    }



}
