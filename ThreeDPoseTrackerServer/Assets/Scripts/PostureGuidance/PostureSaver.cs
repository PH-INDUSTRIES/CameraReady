﻿using System;
using System.IO;
using Mirror;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Saves the user's current posture into a json-file that is compatible with the given Kinect structure
/// </summary>
public class PostureSaver : NetworkBehaviour
{
    // avatar
    public GameObject model;
    // animator holds all avatar transforms
    private Animator anim;
    private KinectJoint[] joints;
    private PostureLoader.Posture posture;
    private PostureLoader pl;
    
    // short white screen for simulating a camera shutter animation
    public Image whitescreen;
    private bool whiteframe = false;

    // (DEPRECATED) network save countdown
    private float saveCountdown = 0f;
    private bool saveCountEnabled;
    
    public Text waitText;
    public Text neutralText;

    private bool ready = false;// ready is true if the neural network has started
    private float counter = 0;// time counter for the neutral pose
    private bool neutral = false;// true, if neutral is shown
    private float waitTime = 2.5f;// max time the neutral pose is shown between postures
    private bool startposture = true;// time measured hwo long one user needs for one posture
    private float postureTime = 0f;// true at the start of the program before user input
    
    // file directory for saving json
    private DirectoryInfo dir;
    private DirectoryInfo imagesDir;

    // needed for saving photos
    public UIScript UiScript;
    public VideoCapture VideoCapture;


    public class KinectJoint
    {
        public Vector3 pos = new Vector3();
        public string name;

        public KinectJoint(Vector3 pos, string name)
        {
            this.pos = pos;
            this.name = name;
        }
    }


        // Start is called before the first frame update
    void Start()
    {
        // create a save directory that is always different (dependent on system time)
        dir = Directory.CreateDirectory(System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + "/Posture Guidance/" +
                                        GetTimeInSeconds());
        imagesDir = Directory.CreateDirectory(dir.FullName+"/images");
        // Write down additional information in a text file in the same path
        System.IO.File.WriteAllText(dir.FullName+"/variables.txt", "Version: "+(Variables.Version+1)+"\nNumber of Postures: "+Variables.PostureArray.Count+"\nDisplay: Tablet\n");


        pl = GetComponent<PostureLoader>();
        anim = model.GetComponent<Animator>();
        joints = new KinectJoint[25];
    }

    // Update is called once per frame
    void Update()
    {
        // add elapsed time during posture phase
        if (ready)
        {
            postureTime += Time.deltaTime;
        }
        
        
        // checks if the neural network initialized
        // if it starts tracking, avatar position is never exactly (0,0,0)
        if (!ready && !(model.transform.position == Vector3.zero))
        {
            ready = true;
            waitText.gameObject.SetActive(false);
            UiScript.RpcChangeWaitTextStatus(false);
        }
        
        // a white frame is only shown one frame
        if (whiteframe)
        {
            whitescreen.gameObject.SetActive(false);
        }

        // add elapsed time during neutral phase
        if (neutral)
        {
            counter += Time.deltaTime;
        }

        // change to next posture after neutral time threshold
        if (counter>waitTime)
        {

            if (Variables.Posture < Variables.PostureArray.Count)
            {
                // tell clients about neutral text
                UiScript.RpcChangeNeutralText(false);
                neutral = false;
                counter = 0;
                pl.changePosture(false);
                postureTime = 0f;
                neutralText.gameObject.SetActive(false);
            }
            else
            {
                waitText.gameObject.SetActive(true);
                // tell clients about neutral text
                UiScript.RpcChangeWaitTextStatus(true);
            }
            
        }
        
        // handle user input
        if ((Input.GetKeyDown(KeyCode.PageDown)|| Input.GetKeyDown(KeyCode.PageUp)) )
        {
            // save posture if ready and not neutral
            if (!startposture&&!neutral && ready)
            {
                SavePosture();

                Variables.Posture++;
                whitescreen.gameObject.SetActive(true);
                whiteframe = true;
                pl.changePosture(true);

                neutral = true;
                neutralText.gameObject.SetActive(true);
                // tell clients about neutral text
                UiScript.RpcChangeNeutralText(true);

                // save the posture completion time into the parameters text file
                System.IO.File.AppendAllText(dir.FullName + "/variables.txt", "Posture " + Variables.Posture.ToString("D3") + " completion time: " + postureTime + "\n");
            }

            // only used for the beginning of the program
            if (startposture&&ready)
            {
                pl.changePosture(false);
                neutral = false;
                counter = 0;
                postureTime = 0f;
                startposture = false;
            }
        }
    }
    
    
    
    /// <summary>
    /// Defines the recursive linking of all joints and gets all transforms
    /// </summary>
    public void SavePosture()
    {
        joints[0] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.Hips).position,"SPINEBASE");
        joints[1] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.Spine).position,"SPINEMID");
        joints[2] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.Neck).position,"NECK");
        joints[3] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.Head).position,"HEAD");
        
        joints[4] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.LeftUpperArm).position,"SHOULDERRIGHT");
        joints[5] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.LeftLowerArm).position,"ELBOWRIGHT");
        joints[6] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.LeftHand).position,"WRISTRIGHT");
        joints[7] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.LeftMiddleProximal).position,"HANDRIGHT");

        joints[8] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.RightUpperArm).position,"SHOULDERLEFT");
        joints[9] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.RightLowerArm).position,"ELBOWLEFT");
        joints[10] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.RightHand).position,"WRISTLEFT");
        joints[11] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.RightMiddleProximal).position,"HANDLEFT");
        
        joints[12] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.LeftUpperLeg).position,"HIPRIGHT");
        joints[13] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.LeftLowerLeg).position,"KNEERIGHT");
        joints[14] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.LeftFoot).position,"ANKLERIGHT");
        joints[15] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.LeftToes).position,"FOOTRIGHT");
        
        joints[16] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.RightUpperLeg).position,"HIPLEFT");
        joints[17] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.RightLowerLeg).position,"KNEELEFT");
        joints[18] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.RightFoot).position,"ANKLELEFT");
        joints[19] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.RightToes).position,"FOOTELFT");
        
        joints[20] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.Chest).position,"SPINESHOULDER");

        joints[21] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.LeftMiddleDistal).position,"HANDTIPRIGHT");
        joints[22] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.LeftThumbDistal).position,"THUMBRIGHT");
        
        joints[23] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.RightMiddleDistal).position,"HANDTIPLEFT");
        joints[24] = new KinectJoint(anim.GetBoneTransform(HumanBodyBones.RightThumbDistal).position,"THUMBLEFT");
        

        //FOOTELFT written wrong on purpose to enable compatibility with given postures
        // Recursive linking of all joints
        PostureLoader.Posture FOOTELFT = createPosture(15, null);
        PostureLoader.Posture ANKLELEFT = createPosture(14, new []{FOOTELFT});
        PostureLoader.Posture KNEELEFT = createPosture(13, new []{ANKLELEFT});
        PostureLoader.Posture HIPLEFT = createPosture(12, new []{KNEELEFT});
        
        PostureLoader.Posture HANDTIPLEFT = createPosture(21, null);
        PostureLoader.Posture HANDLEFT = createPosture(7, new []{HANDTIPLEFT});
        PostureLoader.Posture THUMBLEFT = createPosture(22, null);
        PostureLoader.Posture WRISTLEFT = createPosture(6, new []{THUMBLEFT,HANDLEFT});
        PostureLoader.Posture ELBOWLEFT = createPosture(5, new []{WRISTLEFT});
        PostureLoader.Posture SHOULDERLEFT = createPosture(4, new []{ELBOWLEFT});
        
        PostureLoader.Posture HEAD = createPosture(3, null);
        PostureLoader.Posture NECK = createPosture(2, new []{HEAD});

        PostureLoader.Posture HANDTIPRIGHT = createPosture(23, null);
        PostureLoader.Posture HANDRIGHT = createPosture(11, new []{HANDTIPRIGHT});
        PostureLoader.Posture THUMBRIGHT = createPosture(24, null);
        PostureLoader.Posture WRISTRIGHT = createPosture(10, new []{THUMBRIGHT,HANDRIGHT});
        PostureLoader.Posture ELBOWRIGHT = createPosture(9, new []{WRISTRIGHT});
        PostureLoader.Posture SHOULDERRIGHT = createPosture(8, new []{ELBOWRIGHT});
        
        PostureLoader.Posture SPINESHOULDER = createPosture(20, new []{SHOULDERLEFT,NECK,SHOULDERRIGHT});
        PostureLoader.Posture SPINEMID = createPosture(1, new []{SPINESHOULDER});
        
        PostureLoader.Posture FOOTRIGHT = createPosture(19, null);
        PostureLoader.Posture ANKLERIGHT = createPosture(18, new []{FOOTRIGHT});
        PostureLoader.Posture KNEERIGHT = createPosture(17, new []{ANKLERIGHT});
        PostureLoader.Posture HIPRIGHT = createPosture(16, new []{KNEERIGHT});
        
        PostureLoader.Posture SPINEBASE = createPosture(0, new []{HIPLEFT,SPINEMID,HIPRIGHT});
        
        SaveIntoJson(SPINEBASE);
    }

    /// <summary>
    /// Save the whole posture as a json-file with the same layout as the input
    /// </summary>
    /// <param name="posture">SPINEBASE joint</param>
    public void SaveIntoJson(PostureLoader.Posture posture)
    {
        string output = JsonUtility.ToJson(posture,true);
        string postureint = (Variables.Posture + 1).ToString("D3");
        string posturearrayint = (Variables.PostureArray[Variables.Posture] + 1).ToString("D3");
        System.IO.File.WriteAllText(dir.FullName+"/["+postureint+"]"+posturearrayint+"_posture.json",output);
        
        // Also save a photo
        VideoCapture.SaveTextureAsJPG(imagesDir);
    }

    /// <summary>
    /// This serves the purpose of giving always different strings (dependent on system time)
    /// </summary>
    /// <returns>system time in seconds</returns>
    private string GetTimeInSeconds()
    {
        DateTime now = System.DateTime.Now;
        return now.Year.ToString() + now.Month.ToString()+now.Day.ToString()+now.Hour.ToString()+now.Minute.ToString()+now.Second.ToString();
    }

    /// <summary>
    /// Creates a new Posture with all its parameters, used for saving
    /// </summary>
    /// <param name="joint_index">index of "joints" array (between 0 and 24)</param>
    /// <param name="child_joints">new array of all child joints</param>
    /// <returns>Kinect-compatible posture parameters</returns>
    private PostureLoader.Posture createPosture(int joint_index, PostureLoader.Posture[] child_joints)
    {
        return new PostureLoader.Posture(joints[joint_index].name,joints[joint_index].pos,child_joints);
    }
    
    
}
