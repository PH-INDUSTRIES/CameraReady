﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Mirror;
using TextureSendReceive;
using UnityEngine;
using UnityEngine.SceneManagement;


public enum VideoEncoding
    {
        JPG = 0,
        PNG = 1
    }


public class PlayerScript : NetworkBehaviour
    {

        private int timeCount;
        private float pauseTime;
        private float maxPause = 5f;
        private bool pause = false;
        private int updateInterval = 15;


        private VideoCapture videocapture;

        //------------------------------------------------Webcam receiver parameters----------------------------------------
        Texture2D source;
        private TcpListener listner;
        private List<TcpClient> clients = new List<TcpClient>();
        private bool stop = false;

        public int port = 5000;
        public VideoEncoding encoding = VideoEncoding.JPG;


        public int messageByteLength = 24;
        //------------------------------------------------------------------------------------------------------------------
        
        private bool initiated = false;
        private WebcamManager _webcamManager;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (SceneManager.GetActiveScene().buildIndex != 1)
            {
                return;
            }
            
            // initiate Webcam receiver and texture used for neural network evaluation
            if (isServer && !initiated)
            {
                GameObject.Find("WebcamReceiver").GetComponent<MyReceiverScript>().InitiateReceiver();
                GameObject.Find("MainTexrure").GetComponent<VideoCapture>().SetMainTextureDimensions();
                initiated = true;
            }
        
        
            // start 
            if (isLocalPlayer&&!isServer&&!initiated)
            {
                _webcamManager = GameObject.Find("WebcamManager").GetComponent<WebcamManager>();
                //_webcamManager.GetComponent<WebcamSender>().InitWebcam();
            
            
                Application.runInBackground = true;

                source = new Texture2D(1280,720);

                source = _webcamManager.SetSelectedWebcam();

                //Start coroutine for TCP listener
                StartCoroutine(initAndWaitForTexture());
            
            
                initiated = true;
            }
            

            if (!isLocalPlayer || isServer)
            {
                return;
            }

            if (pause)
            {
                pauseTime += Time.deltaTime;
            }

            if (pauseTime > maxPause)
            {
                pause = false;
            }

        }


        //------------------------------------------------------------------------------------------------------------------


        //Converts the data size to byte array and put result to the fullBytes array
        void byteLengthToFrameByteArray(int byteLength, byte[] fullBytes)
        {
            //Clear old data
            Array.Clear(fullBytes, 0, fullBytes.Length);
            //Convert int to bytes
            byte[] bytesToSendCount = BitConverter.GetBytes(byteLength);
            //Copy result to fullBytes
            bytesToSendCount.CopyTo(fullBytes, 0);
        }

        /// <summary>
        /// Co-routine for TCP Listening
        /// </summary>
        IEnumerator initAndWaitForTexture()
        {
            while (source == null)
            {
                yield return null;
            }

            // Connect to the server
            listner = new TcpListener(IPAddress.Any, port);

            listner.Start();

            //Start sending coroutine
            StartCoroutine(senderCOR());
        }

        WaitForEndOfFrame endOfFrame = new WaitForEndOfFrame();

        /// <summary>
        /// Co-routine for TCP webcam feed sending
        /// </summary>
        IEnumerator senderCOR()
        {
            bool isConnected = false;
            TcpClient client = null;
            NetworkStream stream = null;

            // Wait for client to connect in another Thread 
            Loom.RunAsync(() =>
            {
                while (!stop)
                {
                    // Wait for client connection
                    client = listner.AcceptTcpClient();
                    // We are connected
                    clients.Add(client);

                    isConnected = true;
                    stream = client.GetStream();
                }
            });

            //Wait until client has connected
            while (!isConnected)
            {
                yield return null;
            }

            bool readyToGetFrame = true;

            byte[] frameBytesLength = new byte[messageByteLength];

            while (!stop)
            {
                //Wait for End of frame
                yield return endOfFrame;
                byte[] imageBytes = EncodeImage();

                //Fill total byte length to send. Result is stored in frameBytesLength
                byteLengthToFrameByteArray(imageBytes.Length, frameBytesLength);

                //Set readyToGetFrame false
                readyToGetFrame = false;

                Loom.RunAsync(() =>
                {
                    //Send total byte count first
                    stream.Write(frameBytesLength, 0, frameBytesLength.Length);

                    //Send the image bytes
                    stream.Write(imageBytes, 0, imageBytes.Length);
                    //Sent. Set readyToGetFrame true
                    readyToGetFrame = true;
                });

                //Wait until we are ready to get new frame(Until we are done sending data)
                while (!readyToGetFrame)
                {
                    yield return null;
                }
            }
        }

        /// <summary>
        /// gets webcam texture and returns byte array as jpg or png format
        /// </summary>
        private byte[] EncodeImage()
        {
            // very costly conversion, requires enough computing power
            if (encoding == VideoEncoding.PNG) return source.EncodeToPNG();
            return source.EncodeToJPG();
        }

        // stop everything
        private void OnApplicationQuit()
        {
            if (listner != null)
            {
                listner.Stop();
            }

            foreach (TcpClient c in clients)
                c.Close();
        }

        //------------------------------------------------------------------------------------------------------------------

    }

