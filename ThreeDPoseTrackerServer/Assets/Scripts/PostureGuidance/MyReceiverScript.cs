﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using Mirror;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script handles the receiving of the webcam feed
/// </summary>
public class MyReceiverScript : NetworkBehaviour
{
    public RawImage image;
        Texture2D Receivetexture;
        
        public int port = 5000;
        public string IP = "127.0.0.1";//not used
        TcpClient client;

        [HideInInspector]
        public Texture2D texture;

        private bool stop = false;

        [Header("Must be the same in sender and receiver")]
        public int messageByteLength = 24;

        // Use this for initialization
        void Start () {

        }

        /// <summary>
        /// Establish connection to the tablet via network
        /// </summary>
        public void InitiateReceiver()
        {
            // texture where webcam feed is set
            Receivetexture = new Texture2D(1280, 720);
            image.texture = Receivetexture;

            SetTargetTexture(Receivetexture);

            
            Application.runInBackground = true;

            client = new TcpClient();

            //Connect to server from another Thread
            Loom.RunAsync(() => {                
                // if on desktop
                // client.Connect(IPAddress.Loopback, port);
                
                // Get IP address from Mirror-Framework
                Dictionary<int,NetworkConnectionToClient> connections = NetworkServer.connections;

                string player2 = connections[1].address.Replace("::ffff:", "");
                Debug.Log(player2);
                
                
                client.Connect(IPAddress.Parse(player2), port);

                imageReceiver();
            });
        }
        
        
        
        
                void imageReceiver() {
            //While loop in another Thread is fine so we don't block main Unity Thread
            Loom.RunAsync(() => {
                while (!stop) {
                    //Read Image Count
                    int imageSize = readImageByteSize(messageByteLength);

                    //Read Image Bytes and Display it
                    readFrameByteArray(imageSize);
                }
            });
        }


         /// <summary>
         ///  Converts the byte array to the data size and returns the result
         /// </summary>
         /// <param name="frameBytesLength"></param>
         /// <returns>frame size</returns>
        int frameByteArrayToByteLength(byte[] frameBytesLength) {
            int byteLength = BitConverter.ToInt32(frameBytesLength, 0);
            return byteLength;
        }

        private int readImageByteSize(int size) {
            bool disconnected = false;

            NetworkStream serverStream = client.GetStream();
            byte[] imageBytesCount = new byte[size];
            var total = 0;
            do {
                var read = serverStream.Read(imageBytesCount, total, size - total);
                if (read == 0)
                {
                disconnected = true;
                break;
                }
                total += read;
            } while (total != size);

            int byteLength;

            if (disconnected) {
                byteLength = -1;
            } else {
                byteLength = frameByteArrayToByteLength(imageBytesCount);
            }

            return byteLength;
        }

        /// <summary>
        /// get image patch sizes
        /// </summary>
        /// <param name="size">pre-defined set length</param>
        private void readFrameByteArray(int size) {
            bool disconnected = false;

            NetworkStream serverStream = client.GetStream();
            byte[] imageBytes = new byte[size];
            var total = 0;
            do {
                var read = serverStream.Read(imageBytes, total, size - total);
                if (read == 0)
                {
                disconnected = true;
                break;
                }
                total += read;
            } while (total != size);

            bool readyToReadAgain = false;

            //Display Image
            if (!disconnected) {
                //Display Image on the main Thread
                Loom.QueueOnMainThread(() => {
                    loadReceivedImage(imageBytes);
                    readyToReadAgain = true;
                });
            }

            //Wait until old Image is displayed
            while (!readyToReadAgain) {
                System.Threading.Thread.Sleep(1);
            }
        }


        void loadReceivedImage(byte[] receivedImageBytes) {
            if(texture) texture.LoadImage(receivedImageBytes);
        }

        /// <summary>
        /// Set which texture in the Unity project should show the image
        /// </summary>
        /// <param name="t">Unity texture element</param>
        public void SetTargetTexture (Texture2D t) {
            texture = t;
        }

        void OnApplicationQuit() {
            stop = true;

            if (client != null) {
            client.Close();
            }
        }
}
