﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// This script handles the user input before starting the main program
/// </summary>
public class MainMenuScript : MonoBehaviour
{
    public Dropdown dropdown;
    public InputField input;
    public Text posturesText;
    public Dropdown cameras;
    public Dropdown drop_display;
    
    // Start is called before the first frame update
    void Start()
    {
        WebCamDevice[] devices = WebCamTexture.devices;
        foreach (var d in devices)
        {
            cameras.options.Add(new Dropdown.OptionData(d.name));
        }
        cameras.value = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Saves the visualisations choice in the Variables script
    /// </summary>
    public void changeVis()
    {
        Variables.Version = dropdown.value;
    }


    /// <summary>
    /// This is a legacy function used for generating a random amount of postures dependent on a user given amount
    /// (This function is not used due to having a defined set of postures)
    /// </summary>
    public void onGeneratePostures()
    {
        List<int> postures = new List<int>();
        int amount = int.Parse(input.text);
        if (amount > 0 && amount < 20)
        {
            while (postures.Count<amount)
            {
                int rand = Random.Range(0, 19);
                if (postures.Count == 0 || !postures.Contains(rand))
                {
                    postures.Add(rand);
                }
            }

            string posturesString = "";
            foreach (var posture in postures)
            {
                posturesString += (posture+1).ToString() + " ,";
            }

            posturesText.text = posturesString;
            Variables.PostureArray = postures;
        }
        
    }

    /// <summary>
    /// Saves the display choice in the Variables script
    /// (This was necessary for the user study)
    /// </summary>
    public void onChangeDisplay()
    {
        Variables.Diplay = drop_display.value;
    }

    public void onStart()
    {
        SceneManager.LoadScene("MainScene");
    }


    
    /// <summary>
    /// Initializes the main program and changes to the main scene
    /// </summary>
    public void onNewStart()
    {
        // Postures are copied in a new array
        List<int> postures = new List<int>();
        int amount = 20;
        for (int i = 0; i < amount; i++)
        {
            postures.Add(i);
        }

        int posTemp = 0;
        
        // Postures are randomly shuffled
        for (int i = 0; i < postures.Count; i++) {
            int rnd = Random.Range(0, postures.Count);
            posTemp = postures[rnd];
            postures[rnd] = postures[i];
            postures[i] = posTemp;
        }
        
        // Postures are copied to the Variables array
        Variables.PostureArray = postures;

        foreach (var VARIABLE in Variables.PostureArray)
        {
            Debug.Log(VARIABLE);
        }    

        // Load the Main Scene
        SceneManager.LoadScene("MainScene");
    }

    /// <summary>
    /// Saves the camera choice in the Variables script
    /// (Cameras are set in the Start function)
    /// </summary>
    public void onChangeCamera()
    {
        Variables.Camera = cameras.value;
    }
    
    
}
