﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// (Deprecated) Handles the Popup-window for changing visualisations
/// </summary>
public class PostureChangeScript : MonoBehaviour
{
    public GameObject panel;
    public InputField inf_InputField;
    public PostureLoader pl;



    
    
    // Start is called before the first frame update
    void Start()
    {
        panel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// (Deprecated) Activate Visualisation Popup-window
    /// </summary>
    public void activatePanel()
    {
        panel.SetActive(true);
    }

    /// <summary>
    /// (Deprecated) For Testing purposes, set a specific posture from the posture array via index
    /// </summary>
    public void changePosture()
    {
        int pos = int.Parse(inf_InputField.text);
        if (pos > 0 && pos < 41)
        {
            Variables.Posture = pos - 1;

            //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            pl.changePosture(false);
            panel.SetActive(false);
        }
    }

    /// <summary>
    /// (Deprecated) Change to the Stick-Figure visualisation and reload posture via the Popup-window
    /// </summary>
    public void changeVis1()
    {
        Variables.Version = 0;
        pl.changePosture(false);
        panel.SetActive(false);
    }
    /// <summary>
    /// (Deprecated) Change to the Mesh visualisation and reload posture via the Popup-window
    /// </summary>
    public void changeVis2()
    {
        Variables.Version = 1;
        pl.changePosture(false);
        panel.SetActive(false);
    }
    /// <summary>
    /// (Deprecated) Change to the Outline visualisation and reload posture via the Popup-window
    /// </summary>
    public void changeVis3()
    {
        Variables.Version = 2;
        pl.changePosture(false);
        panel.SetActive(false);
    }
    
}
