﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Variables
{
    private static int version = 0;
    private static int posture = 0;
    private static List<int> postureArray = new List<int>(){0};
    private static int camera = 0;
    private static int diplay = 0;

    /// <summary>
    /// User study display type
    /// </summary>
    public static int Diplay
    {
        get => diplay;
        set => diplay = value;
    }

    /// <summary>
    /// index of camera array
    /// </summary>
    public static int Camera
    {
        get => camera;
        set => camera = value;
    }

    /// <summary>
    /// list of postures
    /// </summary>
    public static List<int> PostureArray
    {
        get => postureArray;
        set => postureArray = value;
    }

    /// <summary>
    /// 0=stick-figure, 1=mesh, 2=outline
    /// </summary>
    public static int Version
    {
        get => version;
        set => version = value;
    }

    /// <summary>
    /// index of posture-array
    /// </summary>
    public static int Posture
    {
        get => posture;
        set => posture = value;
    }
}
